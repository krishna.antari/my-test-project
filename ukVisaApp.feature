Scenario: Japanese citizen intending to visit UK as a tourist
	Given I provide a nationality of Japan
	And I select the reason �Tourism�
	When I submit the form
	Then I will be informed �I won�t need a visa to study in the UK� 


Scenario: Russian citizen intending to visit UK as tourist
	Given I provide a nationality of Russia
	And I select the reason �Tourism�
	And I state I am not travelling or visiting a partner or family
	When I submit the form
	Then I will be informed �I need a visa to come to the UK�
